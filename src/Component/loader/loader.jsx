import React from 'react';
import {Box, CircularProgress, Stack} from "@mui/material";

function Loader() {
  return (
    <Box minHeight={'90vh'}>
      <Stack
        direction={"row"}
        justifyContent={"center"}
        alignItems={"center"}
        height={'100%'}
      >
        <CircularProgress/>
      </Stack>
    </Box>
  );
}

export default Loader;
