import React from 'react';
import {Box, Stack} from "@mui/material";
import {ChannelCard, Loader, VideoCard} from "../index";

function Videos({videos}) {
    if (!videos.length){
      return <Loader/>
    }
  return (
    <Stack
      width={"100%"}
      direction={"row"}
      flexWrap={"wrap"}
      justifyContent={"start"}
      alignItems={"center"}
      gap={2}
    >
      {videos.map((items , idx) => (
        <Box key={idx}>
          {items.id.videoId && <VideoCard video={items}/>}
          {items.id.channelId && <ChannelCard video={items}/>}
        </Box>
      ))}
    </Stack>
  );
}

export default Videos;