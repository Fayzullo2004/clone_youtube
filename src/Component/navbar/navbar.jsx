import { Box, Stack } from "@mui/material";
import { Logo } from "../../constants/svg"
import { colors } from '../../constants/colors'
import { Link } from "react-router-dom";
import { SearchBar } from "../index";

function Navbar() {
  return (
    <Stack
      direction={"row"}
      alignItems={"center"}
      justifyContent={"space-between"}
      p={"2"}
      sx={{position : "sticky", top : 0, zIndex : 999, background: colors.primary }}
    >
      <Link to={'/'}>
        <Logo />
      </Link>
      <SearchBar />
      <Box />
    </Stack>
  );
}

export default Navbar;